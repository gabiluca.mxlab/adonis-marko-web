// Compiled using marko@4.11.3 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require("marko/src/html").t(__filename),
    marko_componentType = "/adonis-fullstack-app$4.1.0/resources/views/welcome.marko",
    components_helpers = require("marko/src/components/helpers"),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c,
    marko_forRange = require("marko/src/runtime/helper-forRange"),
    marko_loadTemplate = require("marko/src/runtime/helper-loadTemplate"),
    layout_template = marko_loadTemplate(require.resolve("./components/layout")),
    marko_helpers = require("marko/src/runtime/html/helpers"),
    marko_loadTag = marko_helpers.t,
    layout_tag = marko_loadTag(layout_template);

function render(input, out, __component, component, state) {
  var data = input;

  layout_tag({
      renderBody: function renderBody(out) {
        marko_forRange(1, 3, 1, function(i) {
          out.w("<section><div class=\"logo\"></div><div class=\"title\"></div><div class=\"subtitle\"><p>AdonisJs simplicity will make you feel confident about your code</p></div></section>");
        });
      }
    }, out, __component, "0");
}

marko_template._ = marko_renderer(render, {
    ___implicit: true,
    ___type: marko_componentType
  });

marko_template.Component = marko_defineComponent({}, marko_template._);

marko_template.meta = {
    id: "/adonis-fullstack-app$4.1.0/resources/views/welcome.marko",
    tags: [
      "./components/layout"
    ]
  };
